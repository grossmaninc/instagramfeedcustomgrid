<?php
namespace GrossmanInteractive\InstagramFeedCustomGrid\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
 
class Type implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
        ['value' => 'content', 'label' => __('Instagram Page')],
        ['value' => 'popup', 'label' => __('Popup Page')]];
    }
}
