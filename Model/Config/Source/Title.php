<?php
namespace GrossmanInteractive\InstagramFeedCustomGrid\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
 
class Title implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
        ['value' => 'no', 'label' => __('No')],
        ['value' => 'yes', 'label' => __('Yes')]];
    }
}
